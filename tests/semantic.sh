#!/bin/bash

curl -v -X PUT localhost:7474/rdf/graph -H "Content-Type:text/turtle" --data-binary @abox.ttl

curl -v -X PUT localhost:7474/rdf/graph\?graph=urn%3Asparqlextension%3Atbox -H "Content-Type:text/turtle" --data-binary @tbox.ttl

curl -v -X POST localhost:7474/rdf/query \
-H "Content-Type: application/sparql-query" \
-H "Accept: text/tab-separated-values" \
-d "PREFIX : <http://comsys.uni-kiel.de/sparql/test/> SELECT ?p1 ?p2 WHERE { ?p1 :hasParentInLaw ?p2 }"

curl -v -X POST localhost:7474/rdf/query/inference \
-H "Content-Type: application/sparql-query" \
-H "Accept: text/tab-separated-values" \
-d "PREFIX : <http://comsys.uni-kiel.de/sparql/test/> \
SELECT ?p1 ?p2 WHERE { ?p1 :hasParentInLaw ?p2 }"

curl -v -X POST localhost:7474/rdf/update \
-H "Content-Type: application/sparql-update" \
-d "PREFIX : <http://comsys.uni-kiel.de/sparql/test/> \
DELETE DATA { :Emily :hasParent :Dave }; \
INSERT DATA { :Emily :hasParent :Bob }"


